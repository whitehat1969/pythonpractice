#!/usr/bin/env python

"""
Good coding style : this doesn't get printed
	1. comments
	2. self documenting identifiers
	3. spaces, tabs, tidiness
	4. Reusable code
"""
# this is a comment

"""
This program will print a list of names
By whitehat
"""

# def = define (define a function)
# greet is the function
# name is something we are going to pass through the function

def greet(name):
	print "hello", name
	print "How are you?"
	
greet("whitehat")
greet("Raspberry Pi")
			

