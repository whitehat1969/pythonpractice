#!/usr/bin/env python
# -*- coding: utf-8 -*-

toys = [ 'big Wheel', 'unicycle', 'bean bags', 'mountain bike', 'legos', 'pc' ]
foods = [ 'banana', 'pancakes', 'jelly beans', 'm&m', 'doritos', 'kit kat' ]
favorites = toys + foods
print favorites

first = "bill"
last = "Jenson"
print "name= " + first + " " + last

age = 43

if age > 35:
	print 'you are too old!'
else:
	print 'you are too young'


age = 12
if age == 10:
	print 'you are 10'
elif age == 11:
	print 'you are 11'
elif age == 12:
	print 'you are 12'
elif age == 13:
	print 'you are 13'
else:
	print 'huh?'

if age == 10 or age == 11 or age == 12 or age == 13:
	print 'you are %s' % age
else:
	print 'huh?'

if age >= 10 and age <= 13:
	print 'you are %s' % age
else:
	print 'huh?'


age = '10'
converted_age = int(age)				# convert a string into a number

for x in range(0, 5):
	print 'hello'

for i in foods:
	print i


mylist = [ 'a', 'b', 'c' ]
for i in mylist:
	print i
	for j in mylist:
		print j

chores = 10
mowing = 10
spending = 5

savings = 0
for week in range(1, 53):
	savings = savings + chores + mowing - spending
	print 'Week %s = %s' % (week, savings)

savings = 600
chores = 10
mowing = 10
spending = 5
week = 1
months = 0
while savings < 2000:
	savings = savings + chores + mowing - spending
	print 'Week %s = %s' % (week, savings)
	week = week + 1

# months = (week/4)
# print "months = " + months

