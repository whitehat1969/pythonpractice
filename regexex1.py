import re

pat = re.compile(r'<[^<]*?/?>')

# Find <
# Not if it is followed by a second < before a closing >
# *? this is known as the lazy quantifier it looks for the smallest match
# it contain a / but that is not needed and then a >

print pat.sub('', 'Click here <h1 class="whatever">4 < 9</h1>')
