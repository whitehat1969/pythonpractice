#! /usr/bin/python

# Whats a tuple? An Immutable List

tupleEx = ('Derek', 35, 'Pittsburgh', 'PA')

for i in tupleEx:
    print(i)

print("First element in the tuple is ", tupleEx[0])

smTuple = (66,)
print smTuple

tupleFunc = tuple('abcde')
print tupleFunc

# That's it tuples don't have methods

listEx = ['Derek', 35, 'Pittsburgh', 'PA']

for i in listEx:
    print(i)
    
# Slicing Up Lists

print listEx[0:2]
print listEx[-1]
listNum = [1,2,3,4,5,6,7,8,9,10]
print listNum[-3:]
print listNum[:3]
print listNum[1:10:2]
print len(listNum)
print min(listNum)
print max(listNum)

listName = list('Fred')
print listName
listName[4:] = list('dy')
print listName

listEx3 =  [1,2,3,4]
listEx3[1] = 5
print listEx3
del listEx3[1]
print listEx3 

# List Methods

listEx.append("Joy")

print(listEx)
print(listEx[4])

listEx.remove("Joy")

print(listEx)

listEx.remove(listEx[3])

print(listEx)

listEx.insert(2, 'PA')

print(listEx)

listEx2 = ['f', 'e', 'c', 'd', 'a', 'b']

listEx2.sort()

print(listEx2)

listEx2.reverse()

print(listEx2)

 
strEx = ''.join(listEx2)
print strEx

listEx3 = [
        ['a','b','c'],
        ['d','e','f'],
        ['g','h','i'],
        ]

print(listEx3[2][1])

# Dictionary's are immutable variables that have a key associated with values

dictEx = ({"Age":35, "Height":"6'3", "Weight":169})

print(dictEx)

print(dictEx.get("Height"))

print(dictEx.items())

print(dictEx.values())

dictEx.pop("Height")

print(dictEx)

# while loop time

#a, b = 1, 11

#while a < b:
#    print(a)
#    a += 1

#for x in [1,2,3,4]:
#    print(x)

#listCycle = [1,2,3,4]

#listCycle[:] = range(1,201)

#for i in listCycle : print(i)

#for i in listCycle :
#    if (i%2) == 0:
#        continue
#    elif (i == 101):
#        break
#    else:
#        print(i)

    
    
