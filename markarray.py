#!/usr/bin/env python

"""
This program takes a list of grades out of 10
and then calulates the average
"""

count = 0	#Declare a variable called count to hold the number of times we have collected a grade
total = 0	# holds a running total
gradeslist = list()

while count <= 7:	# do the statement below 8 times (0 through 7)
	newgrade = input("Input grade out of 10 (" + str(count) + ") ")	# get a new grade from user
	gradeslist.append(newgrade)	# add it to the list of grades
	total = total + newgrade		# add newgrade to the total
	count = count + 1
	
#Note: the function str() converts what is passed to it to a string
#We now have a collection of 8 grades input by the user and a total

average = (total / 8)

print "Average grade = " +str(average) + "/8"
