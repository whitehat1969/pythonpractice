#! /usr/bin/python

# Make a program executable chmod a+x helloworld.py
# ./helloworld.py

# The in Operator
listEx = [4,5,6]
stringEx = "Random String"

boolEx = 4 in listEx
print boolEx

print "String" in stringEx

# The is Operator

x = y = [1,2]
z = [1,2]

print x is y
print x is z
print x is not z

print id(x), id(y), id(z)


# The if Statement

# The switch Statement

"""
    switch (variable_passed) {

    case 'value1':

    // Perform actions

    break;

    default:

    // Perform actions

    break;
"""

yourAge = int(raw_input("How old are you: "))

if (yourAge == 35):
    print "Same as me"
else:
    print "Different than me"
    
if (yourAge == 35):
    print "Same as me"
elif (yourAge > 35):
    print "Your older than me"
else:
    print "Your younger than me"


if (yourAge > 0) and (yourAge < 120):
    if (yourAge == 35):
        print "Same as me"
    else:
        print "Different than me"
    
    if (yourAge == 35):
        print "Same as me"
    elif (yourAge > 35):
        print "Your older than me"
    else:
        print "Your younger than me"
else: 
    print "Don't lie about your age"


# The Conditional Expression

x, y = 1, 0
a = 'y is less than x' if (y < x) else 'x is less than y'
print a

