#! /usr/bin/python
import sqlite3

createDb = sqlite3.connect('customers1.db')
queryCurs = createDb.cursor()

def createDatabase():
    
    queryCurs.execute('''
    CREATE TABLE customers(
        id       INTEGER      PRIMARY KEY,
        first    TEXT,
        last     TEXT,
        street   TEXT,
        city     TEXT,
        state    TEXT,
        zip      INTEGER,
        phone    TEXT,
        age      REAL,
        balance  REAL 
        )
    ''')
    return
    
def addCust(first,last,street,city,state,zip,phone,age,balance):
    queryCurs.execute("""
    insert into customers values (first,last,street,city,state,zip,phone,age,balance) values (?,?,?,?,?,?,?,?,?)
    """,first,last,street,city,state,zip,phone,age,balance)
    
    # print queryCurs
    return

def getCust(lastName):
    pass
    #queryCurs.execute('''
    #SELECT * FROM customers
    #''')
    #return


def main():
    
    #createDatabase()
    addCust('Derek','Banas','5708 Highway Ave','Verona','PA',15147,'412-848-5555',35,150.76)
    getCust('Banas')
    
    createDb.commit()
    createDb.close()

if __name__ == '__main__': main()