#!/usr/bin/python
#~ import strings

aList = [123, 'xyz', 'zara', 'abc', 123];
bList = [123, 'OSVDB', 'zaraOSVDB', ': ', 123];
#~ bList = ["192.168.69.128","80","GET : Retrieved x-powered-by header: PHP/5.2.4-2ubuntu5.10"];

print "Count for 123 : ", aList.count(123);
print "Count for zara : ", aList.count('zara');


testcount = bList.count('OSVDB');
print testcount
print "Count for OSVDB : ", bList.count('OSVDB');
#~ print "Count for OSVDB 2 : ", bList.count(bList.startswith( 'OSVDB' ));


#~ testquery = bList.startswith( 'OSVDB' )
#~ print testquery


bList.find('OS') == 0

#~ with

bList.startswith('OS')


x = ['ben','sandy','roger','hillary','john','betty']
print x
#~ How to get indexes of all names which begin with "b"

#~ How about a list comprehension?

x = ['ben','sandy','roger','hillary','john','betty']
[name for name in x if name.find('b') == 0]
#~ [name for name in x if name.find('er') = 0]