#!/usr/bin/python


##### Imports #####

import platform
import time

##### Color Definitions #####

txtblk='\033[0;30m' # Black - Regular
txtred='\033[0;31m' # Red - Regular    
txtgrn='\033[0;32m' # Green - Regular
txtylw='\033[0;33m' # Yellow - Regular
txtblu='\033[0;34m' # Blue - Regular
txtpur='\033[0;35m' # Purple - Regular
txtcyn='\033[0;36m' # Cyan - Regular
txtwht='\033[0;37m' # White - Regular
bldblk='\033[1;30m' # Black - Bold
bldred='\033[1;31m' # Red - Bold
bldgrn='\033[1;32m' # Green - Bold
bldylw='\033[1;33m' # Yellow - Bold
bldblu='\033[1;34m' # Blue - Bold
bldpur='\033[1;35m' # Purple - Bold
bldcyn='\033[1;36m' # Cyan - Bold
bldwht='\033[1;37m' # White - Bold
undblk='\033[4;30m' # Black - Underline
undred='\033[4;31m' # Red - Underline
undgrn='\033[4;32m' # Green - Underline
undylw='\033[4;33m' # Yellow - Underline
undblu='\033[4;34m' # Blue - Underline
undpur='\033[4;35m' # Purple - Underline
undcyn='\033[4;36m' # Cyan - Underline
undwht='\033[4;37m' # White - Underline
bakblk='\033[40m'   # Black - Background
bakred='\033[41m'   # Red - Background
bakgrn='\033[42m'   # Green - Background
bakylw='\033[43m'   # Yellow - Background
bakblu='\033[44m'   # Blue - Background
bakpur='\033[45m'   # Purple - Background
bakcyn='\033[46m'   # Cyan - Background
bakwht='\033[47m'   # White - Background
txtrst='\033[0m'    # Text Reset 

##### Test Platform for Compatibility #####
print "\n Running platform check..."
time.sleep(1)

if platform.system() == 'win32' or platform.system() == 'win64':                
                print "Your platform is incompatible with built in color options."
    print "Try this application on a Linux platform"
    print "Exiting application..."
                exit()
    
else:
    print "You are running an OS compatible with colors!"
                time.sleep(1)

##### Test Colors #####

print "Testing Colors..."
time.sleep(1)

print color(txtblk), "Test - Black", txtrst
print color(txtred), "Test - Red", txtrst
print color(txtgrn), "Test - Green", txtrst
print color(txtylw), "Test - Yellow", txtrst
print color(txtblu), "Test - Blue", txtrst
print color(txtpur), "Test - Purple", txtrst
print color(txtcyn), "Test - Cyan", txtrst
print color(txtwht), "Test - White", txtrst
print color(bldblk), "Test - Bold Black", txtrst
print bldred, "Test - Bold Red", txtrst
print bldylw, "Test - Bold Yellow", txtrst
print bldblu, "Test - Bold Blue", txtrst
print bldpur, "Test - Bold Purple", txtrst
print bldcyn, "Test - Bold Cyan", txtrst
print bldwht, "Test - Bold White", txtrst
print undblk, "Test - Underline Black", txtrst
print undred, "Test - Underline Red", txtrst
print undgrn, "Test - Underline Green", txtrst
print undylw, "Test - Underline Yellow", txtrst
print undblu, "Test - Underline Blue", txtrst
print undpur, "Test - Underline Purple", txtrst
print undcyn, "Test - Underline Cyan", txtrst
print undwht, "Test - Underline White", txtrst
print bakblk, "Test - Background Black", txtrst
print bakred, "Test - Background Red", txtrst
print bakgrn, "Test - Background Green", txtrst
print bakylw, "Test - Background Yellow", txtrst
print bakblu, "Test - Background Blue", txtrst
print bakpur, "Test - Background Purple", txtrst
print bakcyn, "Test - Background Cyan", txtrst
print bakwht, "Test - Background White", txtrst

##### Exiting #####

print "Test complete!"
print "Exiting..."
exit()

##### END #####
