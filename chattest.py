#! /usr/bin/python

# asyncore reads all of the information passed to the defined port
# dispatcher serves as the socket that stands between data sent to the port and the application that will interpret that info
from asyncore import dispatcher 
# Allows you to easily read and write to sockets
from asynchat import async_chat
import socket, asyncore

PORT = 5005
NAME = 'ChatLine'

# Class is used to collect information passed through a socket
class ChatSession(async_chat):
    def __init__(self, server, sock):
        async_chat.__init__(self, sock)
        self.server = server
        self.set_terminator("\r\n")
        self.data = []

    # Called when text is passed through the socket
    def collect_incoming_data(self, data):
        self.data.append(data)

    # Handles the termination of each line of text and then sends it to everyone connected
    def found_terminator(self):
        line = ''.join(self.data)
        self.data = []
        # Sends all the data sent to each client currently connected
        self.server.broadcast(line)
        
    def handle_close(self):
        # Triggered when a user disconnects
        async_chat.handle_close(self)
        self.server.disconnect(self)

class ChatServer(dispatcher):
    def __init__(self, port, name):
        dispatcher.__init__(self)
        # AF_INET uses Transmission Control Protocol as a transport protocol SOCK_STREAM references that this is an actual connection
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM) 
        # Allows you to reuse the same port number even if it wasn't properly closed last time
        self.set_reuse_addr()
        # Binds the socket to the port specified
        self.bind(('', port))
        # Listen for any incoming connections with a maximum of 5 queued connections
        self.listen(5)
        self.name = name
        self.sessions = []
        
    def disconnect(self, session):
        self.sessions.remove(session)
        
    def broadcast(self, line):
        #Send all messages sent to everyone who is connected
        for session in self.sessions:
            session.push(line + '\r\n')

    # Called when the server accepts a new connection
    def handle_accept(self):
        # Allows the new client to connect to the server and is assigned a special socket
        conn, addr = self.accept()
        # Adds the new session to the list
        self.sessions.append(ChatSession(self, conn))

if __name__ == '__main__':
    # The ChatServer keeps a list of all current sessions
    s = ChatServer(PORT, NAME)
    # Monitors the port for any changes over and over
    try: asyncore.loop()
    except KeyboardInterrupt: print