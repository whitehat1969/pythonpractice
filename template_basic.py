#!/usr/bin/python


############################        Imports        ############################

import sys
from subprocess import call


############################       Pre-Sets        ############################

author		= 'whitehat'
description 	= "Insert description here";
Tech			= 'whitehat'		# change this to your name
version		= '0.1.1'

# color options
if sys.platform == 'win32' or sys.platform == 'win64':	
	# if windows, don't use colors
	(r,o,y,g,b) = ('','','','','')
else:
	r 			= '\033[31m' 	#red
	o 			= '\033[0m' 	#off
	y 			= '\033[33m' 	#yellow
	g 			= '\033[32m' 	#green
	b 			= '\033[34m' 	#blue



############################         Menu         #############################

def Main():
	print y, "insert code here", o
	print b, 'colors', r, 'are', g, 'cool', o

	if sys.platform == 'win32' or sys.platform == 'win64':	
		pass
	else:
		call(["chown %s.%s *.xls" %(Tech.lower(), Tech.lower())],shell=True)


############################      Sub-Routines     ############################

def Usage():
	print "\nDescription: " + description 
	print sys.argv[0] +" Version: %s by %s" % (version, author )
	#~ print "\nExample:"
	#~ print "\t" + sys.argv[0] +" -s -I nodes.txt -o output.xls"

if __name__ == '__main__':
	Main()


############################   Revision History   #############################

"""

0.1.1 - based on Password_recheckinator.py
"""


############################   Future Wishlist     ############################

"""

- 

"""


############################          Notes          #############################

"""


"""


############################        The End        ############################

